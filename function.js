function getRequests (num){
    const arr1 = [1,2,3,4,5];
    const arr2 = Array.from(Array(num).keys());


    arr2.reduce((reducer, id) =>
        reducer
            .then(() => Promise.all(
                    arr1.map((id) => fetch (`https://jsonplaceholder.typicode.com/comments/${id}`)
                        .then(
                            resp => resp.json())
                    )                  
                )
                .then(data => {
                    console.log(data, '5 requests completed successfully', `${id}`)
                    for(let i in data){
                         arr1[i] = arr1[i]+5
                    }

                })
                .catch(error => console.log(error))

    ), Promise.resolve())
}

getRequests (100);